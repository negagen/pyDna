import re
import pyDna
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
import pathlib

from functools import reduce

root = tk.Tk('pyDNA')

dna = ""

pathFrm = ttk.Frame(root, padding=10)
pathFrm.pack(fill='x')

pathText = tk.Text(pathFrm, height=1, width=40)
#pathText.grid(column=0,  columnspan=6, row=1)


ttk.Label(pathFrm, text="Ruta del archivo").pack()
pathText.pack(fill='both',expand=True, side='left')


btn1 = ttk.Button(pathFrm, text="Abrir genoma")
btn1.pack(fill='both',expand=True)


transFrame = ttk.Frame(root, padding=10)
transFrame1 = ttk.Frame(transFrame)

dnaFrame = ttk.Frame(transFrame1)

dnaFrame1 = ttk.Frame(dnaFrame)
dnaFrame1.pack(fill='x', ipadx=10)

ttk.Label(dnaFrame1, text="DNA Original").pack()
dnaOriginal1 = tk.Text(dnaFrame1,width=20, height=1)
dnaOriginal1.pack(expand=True)

dnaFrame2 = ttk.Frame(dnaFrame)
dnaFrame2.pack(fill='x', ipadx=10)

ttk.Label(dnaFrame2, text="DNA Complementario").pack()
dnaComplementario = tk.Text(dnaFrame2,width=20, height=1)
dnaComplementario.pack(expand=True)

dnaFrame.grid(row=0,columnspan=2,column=0)


ttk.Label(transFrame1, text="Rango de Transcripción").grid(row=1,columnspan=2,column=0, ipady=5)#.grid(column=0, columnspan=6,  row=4)
from_ = tk.StringVar()
to = tk.StringVar()

from_.set('minimo')
to.set('maximo')

tk.Entry(transFrame1, textvariable=from_).grid(row=2, column=0)#.grid(column=0, columnspan=3, row=5)
tk.Entry(transFrame1, textvariable=to).grid(row=3, column=0)#.grid(column=3, columnspan=3, row=5)
transButton = ttk.Button(transFrame1, text="Transcribir")
transButton.grid(row=2, column=1, ipady=5, rowspan=2)

ttk.Label(transFrame1, text="Rango de Traducción").grid(row=4,columnspan=2,column=0, ipady=5)#.grid(column=0, columnspan=6,  row=4)

from2_ = tk.StringVar()
to2 = tk.StringVar()

from2_.set('minimo')
to2.set('maximo')

tk.Entry(transFrame1, textvariable=from2_).grid(row=5, column=0)#.grid(column=0, columnspan=3, row=5)
tk.Entry(transFrame1, textvariable=to2).grid(row=6, column=0)#.grid(column=3, columnspan=3, row=5)
tradButton = ttk.Button(transFrame1, text="Traducir")
tradButton.grid(row=5, column=1, ipady=5, rowspan=2)

mutFrm = ttk.Frame(transFrame1)
mutFrm.grid(row=7,columnspan=2,column=0, pady=3)

ttk.Label(mutFrm, text="Mutación").pack(fill=tk.X)#.grid(column=0, columnspan=6,  row=4)


deletePos = tk.StringVar(value='posicion')
addPos = tk.StringVar(value='posicion')
addMat = tk.StringVar(value='material')
replacePos = tk.StringVar(value='posicion')
replaceMat = tk.StringVar(value='material')

tk.Entry(mutFrm, textvariable=deletePos).pack(fill=tk.X)
deleteBtn = ttk.Button(mutFrm,text='Eliminar')
deleteBtn.pack(fill=tk.X)
tk.Entry(mutFrm, textvariable=addPos).pack(fill=tk.X)
tk.Entry(mutFrm, textvariable=addMat).pack(fill=tk.X)
addBtn = ttk.Button(mutFrm,text='Añadir')
addBtn.pack(fill=tk.X)
tk.Entry(mutFrm, textvariable=replacePos).pack(fill=tk.X)
tk.Entry(mutFrm, textvariable=replaceMat).pack(fill=tk.X)
replBtn = ttk.Button(mutFrm, text='Remplazar')
replBtn.pack(fill=tk.X)

transFrame2 = ttk.Frame(transFrame)

console = tk.Text(transFrame2)
console.pack(fill='both')

print(console.get(1.0))

transFrame.pack(fill='both')
transFrame1.pack(side=tk.LEFT, padx=10)
transFrame2.pack(fill='both', expand=True)

def eliminar():
    console.insert(tk.END, "Eliminando material genetico\n\n")

    console.insert(tk.END, "RNA Original:\n")
    console.insert(tk.END, re.sub(r'(.{3})',r'\1 ', transcrito))
    console.insert(tk.END, "\n")


    index = int(deletePos.get())
    
    rna_mutado = reduce(lambda prev, next: str(prev) + (next[1] if next[0] != index else ""), enumerate(transcrito), "")

    console.insert(tk.END, "RNA Mutado:\n")
    console.insert(tk.END, re.sub(r"(.{3})", r"\1 ", rna_mutado))
    console.insert(tk.END, "\n")

    proteina = pyDna.proteina(rna_mutado)
    console.insert(tk.END, "Proteina: \n"+ proteina + "\n\n")



    

def agregar():
    console.insert(tk.END, "Agregando material genetico\n\n")

    console.insert(tk.END, "RNA Original:\n")
    console.insert(tk.END, re.sub(r'(.{3})',r'\1 ', transcrito))
    console.insert(tk.END, "\n")


    index = int(addPos.get())
    
    rna_mutado = reduce(lambda prev, next: str(prev) + (next[1] if next[0] != index else (str(next[1])+addMat.get())), enumerate(transcrito), "")

    console.insert(tk.END, "RNA Mutado:\n")
    console.insert(tk.END, re.sub(r"(.{3})", r"\1 ", rna_mutado))
    console.insert(tk.END, "\n")

    proteina = pyDna.proteina(rna_mutado)
    console.insert(tk.END, "Proteina: \n"+ proteina + "\n\n")


def remplazar():
    console.insert(tk.END, "Remplazando material genetico\n\n")

    
    console.insert(tk.END, "RNA Original:\n")
    console.insert(tk.END, re.sub(r'(.{3})',r'\1 ', transcrito))
    console.insert(tk.END, "\n")


    index = int(addPos.get())

    rna_mutado = reduce(lambda prev, next: str(prev) + (next[1] if next[0] != index else replaceMat.get()), enumerate(transcrito), "")

    console.insert(tk.END, "RNA Mutado:\n")
    console.insert(tk.END, re.sub(r"(.{3})", r"\1 ", rna_mutado))
    console.insert(tk.END, "\n")

    proteina = pyDna.proteina(rna_mutado)
    console.insert(tk.END, "Proteina: \n"+ proteina + "\n\n")

def traducir():
    global transcrito

    init = int(from2_.get())
    end = int(to2.get())
    cantidad = end - init

    console.insert(tk.END, "Realizando traducción!\n\n")

    original = dna[init:end]
    transcrito = pyDna.transcribir(dna[init:end])
    
    console.insert(tk.END, "RNA: \n"+re.sub(r"(.{3})", r"\1 ", transcrito) + "\n\n")

    proteina = pyDna.proteina(transcrito)
    guanina = len(re.findall(r"G", original))/len(original)*100
    citocina = len(re.findall(r"C", original))/len(original)*100

    console.insert(tk.END, "Proteina: \n"+ proteina + "\n\n")
    console.insert(tk.END, "Porcenta de citocina: "+ "{c:.1f}%".format(c=citocina)  + "\n")
    console.insert(tk.END, "Porcenta de guanina: "+ "{g:.1f}%".format(g=guanina)  + "\n\n")

    console.insert(tk.END, "Traducción realizada!\n\n")




def transcribe():
    init = int(from_.get())
    end = int(to.get())
    cantidad = end - init

    console.delete(1.0, tk.END)
    console.insert(tk.END, "Realizando transcripción!\n\n")

    original = dna[init:end]
    
    console.insert(tk.END, "DNA original:\n "+re.sub(r"(.{3})", r"\1 ", original) + "\n\n")
    #console.insert(tk.END, "DNA original:\n "+" ".join(re.findall(r".{3}", original)) + "\n\n")

    transcrito = pyDna.transcribir(dna[init:end])

    console.insert(tk.END, "DNA transcrito:\n "+ re.sub(r"(.{3})", r"\1 ", transcrito) + "\n\n")
    #console.insert(tk.END, "DNA transcrito:\n "+ " ".join(re.findall(r".{3}", transcrito)) + "\n\n")

    guanina = len(re.findall(r"G", original))/len(original)*100
    citocina = len(re.findall(r"C", original))/len(original)*100

    console.insert(tk.END, "Transcripcion realizada!\n")


#btn2 = ttk.Button(frm, command=transcribe, text="Transcribir", state=DISABLED)

def selectFile():
    global dna

    file = fd.askopenfile(
        title="Abrir archivo",
        initialdir=pathlib.Path(),
    )

    pathText.insert(tk.END, file.name)

    for line in file.readlines():
        dna += line.replace("\n","")
    
    dnaOriginal1.insert(tk.END, re.sub(r"(.{3})", r"\1 ", dna[:15]))
    #dnaOriginal1.insert(tk.END, " ".join(re.findall(r".{3}",dna[:15])))
    dnaComplementario.insert(tk.END, re.sub(r"(.{3})", r"\1 ", pyDna.hallar_complementario(dna[:15])))
    #dnaComplementario.insert(tk.END, " ".join(re.findall(r".{3}", pyDna.hallar_complementario(dna[:15]))))

    porcentaje_de_guanina = len(re.findall("G", dna))/len(dna)*100
    porcentaje_de_citocina = len(re.findall("C", dna))/len(dna)*100

    #btn2["state"]=NORMAL
    
    file.close()

    #for linea in file.readlines():
        #textContent.insert(END, linea+"\n")
        #textContent.insert(END, pyDna.transcribe(linea)+"\n")



#btn2.pack(fill='x')#.grid(column=6, columnspan=6,row=5)

#ttk.Button(frm, text="Abrir archivo", command=selectFile).grid(column=1, row=1)
#ttk.Button(frm, text="Salir", command=root.destroy).grid(column=2, row=1)

#root.resizable(False, False) 
btn1["command"] = selectFile
transButton["command"] = transcribe
tradButton["command"] = traducir
addBtn["command"] = agregar
deleteBtn["command"] = eliminar
replBtn["command"] = remplazar
 
root.mainloop()